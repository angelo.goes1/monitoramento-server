const { networkInterfaces } = require('os');
const express = require('express')
const {cpu, drive, mem} = require('node-os-utils');

const nets = networkInterfaces();
const results = Object.create(null); // Or just '{}', an empty object

const app = express();

for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
        // 'IPv4' is in Node <= 17, from 18 it's a number 4 or 6
        const familyV4Value = typeof net.family === 'string' ? 'IPv4' : 4
        if (net.family === familyV4Value && !net.internal) {
            if (!results[name]) {
                results[name] = [];
            }
            results[name].push(net.address);
        }
    }
}

const host = results.enp1s0[0];
const port = 8000;

app.get("", async (req, res) => {
    const cpuUsage = await cpu.usage();
    const memUsage = await mem.info();
    const driveUsage = await drive.used();
  
    const payload = {
      cpu: cpuUsage + "%",
      mem: memUsage.usedMemPercentage + "%",
      drive: driveUsage.usedPercentage + "%",
    };
  
    return res.status(200).send(payload);
  });


app.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});

